const startBtn = document.getElementById("start");
const restartBtn = document.getElementById("restart");
const one_emoji = document.getElementById("1");
const two_emoji = document.getElementById("2");
const three_emoji = document.getElementById("3");
const pscore_el = document.getElementById("pscore");
const cscore_el = document.getElementById("cscore");
const result = document.getElementById("result");
const buttonGroup = document.getElementById("button-group");

const option1 = document.getElementById("option1");
const option2 = document.getElementById("option2");

var mode = "playerFirst";
var pscore = 0;
var cscore = 0;
var total = 25;

function setupGame() {
  reset();
  for (let i = 1; i <= total; i++) {
    var image = document.getElementById(`img${i}`);
    image.setAttribute(
      "src",
      "https://icons.iconarchive.com/icons/custom-icon-design/flatastic-11/32/Emoticons-icon.png"
    );
  }
}

function updateView() {
  pscore_el.innerHTML = pscore;
  cscore_el.innerHTML = cscore;
}

function endGame() {
  if (pscore % 2 == 0) result.innerHTML = "You won!";
  else result.innerHTML = "You lost!";
  restartBtn.style.visibility = "visible";
}

function checkAvailableChoices() {
  if (total < 3) {
    three_emoji.style.visibility = "hidden";
    if (total < 2) {
      two_emoji.style.visibility = "hidden";
      if (total < 1) {
        one_emoji.style.visibility = "hidden";
        endGame();
      }
    }
  }
}

function computerMove() {
  switch (mode) {
    case "playerFirst":
      if ((total - 1) % 4 == 0 || (total - 1) % 4 == 1) takeOne("computer")();
      else takeThree("computer")();
      break;
    case "computerFirst":
      if (total >= 22) takeTwo("computer")();
      else if ((total - 1) % 4 == 0 || (total - 1) % 4 == 1)
        takeOne("computer")();
      else takeThree("computer")();
      break;
  }
  checkAvailableChoices();
}

var takeOne = function (subj, callback_f) {
  return function curried_func(e) {
    if (!total) {
      endGame();
      return;
    }
    var elem = document.getElementById("img-container").children[0];

    elem.parentNode.removeChild(elem);
    total--;
    if (subj == "player") pscore++;
    else cscore++;

    updateView();
    if (subj == "player") callback_f();
  };
};

var takeTwo = function (subj, callback_f) {
  return function curried_func(e) {
    if (!total) {
      endGame();
      return;
    }
    for (let i = 0; i < 2; i++) {
      var elem = document.getElementById("img-container").children[0];

      elem.parentNode.removeChild(elem);
      total--;
      if (subj == "player") pscore++;
      else cscore++;
    }
    updateView();
    if (subj == "player") callback_f();
  };
};

var takeThree = function (subj, callback_f) {
  return function curried_func(e) {
    if (!total) {
      endGame();
      return;
    }
    for (let i = 0; i < 3; i++) {
      var elem = document.getElementById("img-container").children[0];

      elem.parentNode.removeChild(elem);
      total--;
      if (subj == "player") pscore++;
      else cscore++;
    }
    updateView();
    if (subj == "player") callback_f();
  };
};

function playGame() {
  one_emoji.addEventListener("click", takeOne("player", computerMove));
  two_emoji.addEventListener("click", takeTwo("player", computerMove));
  three_emoji.addEventListener("click", takeThree("player", computerMove));

  if (document.getElementById("option2").checked) {
    mode = "computerFirst";
    computerMove();
  }
}

function reset() {
  var pscore = 0;
  var cscore = 0;
  var total = 25;
}

startBtn.onclick = function () {
  startBtn.style.visibility = "hidden";
  buttonGroup.style.visibility = "hidden";
  setupGame();
  playGame();
};
